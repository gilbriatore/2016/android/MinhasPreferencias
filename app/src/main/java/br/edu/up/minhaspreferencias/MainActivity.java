package br.edu.up.minhaspreferencias;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        textView = (TextView) findViewById(R.id.textView);

        SharedPreferences sp = getSharedPreferences("minha", 0);
        int id = sp.getInt("cor", 0);
        if (id > 0) {
            cor = cores.get(id);
            linearLayout.setBackgroundColor(cor.fundo);
            textView.setTextColor(cor.frente);
        }


    }


    //onStop


    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences sp = getSharedPreferences("minha", 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("cor", cor.id);
        editor.commit();

    }


    Cor cor;

    public void onClickAlterar(View view) {

        String tag = (String) view.getTag();
        cor = cores.get(Integer.parseInt(tag));

        linearLayout.setBackgroundColor(cor.fundo);
        textView.setTextColor(cor.frente);
    }



    Map<Integer, Cor> cores = new HashMap<>();
    {
        cores.put(1, new Cor(1,Color.BLACK, Color.WHITE)); //branca
        cores.put(2, new Cor(2,Color.WHITE, Color.BLACK)); //preta
        cores.put(3, new Cor(3,Color.BLACK, Color.BLUE));  //azul
        cores.put(4, new Cor(4,Color.BLACK, Color.YELLOW));//amarelo
        cores.put(5, new Cor(5,Color.BLACK, Color.GREEN));  //verde
    }


    private class Cor {

        int id;
        int frente;
        int fundo;

        public Cor(int id, int frente, int fundo){
            this.id = id;
            this.frente = frente;
            this.fundo = fundo;
        }
    }

}
